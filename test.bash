#!/bin/bash
echo "Launching tests"

#### Radiator valves ####

echo "===Test radiator valve==="
#1. Open/close:
./knx_client_script.py raw '0/4/1' 50 2 2
./knx_client_script.py valve set '4/1' 50

#2. Read status:

./knx_client_script.py raw '0/4/1' 0 1 0
./knx_client_script.py valve get '4/1'

#### Window blinds ####

echo "===Test blinds==="
#1. Full open:
echo "#Full open"
./knx_client_script.py raw '1/4/1' 0 1 2
./knx_client_script.py blind open '4/1'

sleep 1
./knx_client_script.py raw '4/4/1' 0 1 0

#2. Full close:
echo "#Full close"
./knx_client_script.py raw '1/4/1'  1 1 2
./knx_client_script.py blind close '4/1'

sleep 2
./knx_client_script.py raw '4/4/1' 0 1 0

#3. Open/close:
echo "#Partial 50%"
./knx_client_script.py raw '3/4/1' 50 2 2
./knx_client_script.py blind set '4/1' 50

sleep 1

#2. Read status:
./knx_client_script.py raw '4/4/1' 0 1 0
./knx_client_script.py blind get '4/1'