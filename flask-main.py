#! /usr/bin/env python3
#-*- coding: utf-8; tab-width: 4 -*-
################################################################################
"""flask_main.py -- a Flask-based server for hepia's LSDS IoT Z-Wave Lab
(frontend)

Documentation is written in `apidoc <http://www.python.org/>`_. You should
already have a copy in `doc/index.html`

To-do
=====

Next milestones:

[ ] Unify get/set route into a single parametric one dispatched over GET or POST.

Bugs
====

None known, many unknown ;-)
"""
################################################################################
import sys, time, logging, os, argparse, re
#import configpi

file_path = os.path.dirname(__file__)
sys.path.insert(0, file_path)

from flask import Flask, jsonify, Response, request
from flask.logging import default_handler
from subprocess import run
import subprocess

my_name = re.split('\.py', __file__)[0]
app = Flask(
    __name__,
    # just for serving apidoc static files
    static_folder=os.path.abspath("doc/"),
    static_url_path=''
)

backend = "./knx_client_script.py"
ResultOK = "Result: OK"



################################################################################

# @app.before_request
def clear_trailing():
    """Remove all repeated '/' -- we don't use schemeless URLs!? BTW, this
    should be fixed with werkzwug v1.x. See
<comnfi# app = Flask(__name__, static_url_path='')
https://github.com/pallets/werkzeug/issues/491> and
<https://stackoverflow.com/questions/40365390/trailing-slash-in-flask-route>
    """
    rp = request.path
    if re.match('//+', rp):
        return redirect(re.sub('/+', '/', rp))


################################################################################
# Global APIdoc defines
################################################################################
"""
@apiDefine          SuccessOK
@apiSuccess         {Object}    ...     JSON array
@apiSuccessExample  {Object}            Success response
    [True, 'OK']
"""

"""
@apiDefine          SuccessOKOldValue
@apiSuccess         {Object}    ...     JSON array
@apiSuccessExample  {Object}            Success response
    [<old-value>, 'OK']
"""

"""
@apiDefine          SuccessJSONArray
@apiSuccess         {Object}    ... JSON associative array. See example below.
"""

"""
@apiDefine          SuccessJSONArrayProduct
@apiSuccess         {Object}    ... JSON associative array keyed by nodes' IDs with
                                    values as nodes' "product names"
"""

"""
@apiDefine          ErrorNoSuchNode
@apiError           QueryFail     Reason: "No such node".
"""

"""
@apiDefine          ErrorQuerySensor
@apiError QueryFail     The query failed. Possible reasons:
                            "No such node",
                            "Not ready",
                            "Not a sensor".
"""

"""
@apiDefine          ErrorQueryDimmer
@apiError QueryFail     The query failed. Possible reasons:
                            "No such node",
                            "Not ready",
                            "Not a dimmer".
"""

#####################################################################################
"""
@api            {get}  /raw send command
@apiName                knx_Raw
@apiGroup               

@apiDescription Send a raw command 

@apiParam {String} group_address
@apiParam {Number} data
@apiParam {Number} datasize
@apiParam {Number} apci


@apiParamExample {Object} Request example
    {
        'group_address' : '0/4/1',
        'data' : '50'
        'datasize' : '2'
        'apci' : '2'
    }

@apiUse     SuccessOKValue

@apiError   WrongInput    Reasons:
                            "Wrong input parameter(s) were provided",
                            "Argument 'location' has incorrect type (expected int, got str).

@apiUse     ErrorQueryDimmer
"""
@app.route('/raw', methods=['GET'], strict_slashes=False)
def knx_raw():
    content = request.get_json()
    if all(
            item in list(content.keys()) for item in [
                'group_address', 'data', 'datasize', 'apci'
            ]
    ):
        group_address = str(content['group_address'])
        data = str(content['data'])
        datasize = str(content['datasize'])
        apci = str(content["apci"])

        command = backend + " " + "raw " + group_address + " " + data + " " + datasize + " " + apci

        output = subprocess.Popen(command, shell = True, stdout=subprocess.PIPE)
        result = str(output.stdout.read())

        return jsonify(result)

    else:
        return ("WrongInput -- Wrong input parameter(s) provided", 400)


#################################################################
"""
@api            {get}  /valve/get get valve value
@apiName                knx_valve_get
@apiGroup               

@apiDescription Send a get command 

@apiParam {String} group_address, No action !!!



@apiParamExample {Object} Request example
    {
        'group_address' : '4/1',
    }

@apiUse     SuccessOKOldValue

@apiError   WrongInput    Reasons:
                            "Wrong input parameter(s) were provided",
                            "Argument 'location' has incorrect type (expected int, got str).

@apiUse     ErrorQueryDimmer
"""
@app.route('/valve/get', methods=['GET'], strict_slashes=False)
def knx_valve_get():
    content = request.get_json()
    if all(
            item in list(content.keys()) for item in [
                'group_address'
            ]
    ):
        group_address = str(content['group_address'])

        command = backend + " " + "valve get " + group_address

        output = subprocess.Popen(command, shell = True, stdout=subprocess.PIPE)
        result = str(output.stdout.read())

        return jsonify(result)

    else:
        return ("WrongInput -- Wrong input parameter(s) provided", 400)


#################################################################
"""
@api            {get}  /valve/set set valve value
@apiName                knx_valve_set
@apiGroup               

@apiDescription Send a set command to a valve 

@apiParam {String} group_address, No action !!!
@apiParam {String} value, set [0,100] in %

@apiParamExample {Object} Request example
    {
        'group_address' : '4/1',
        'value' : '100'
    }

@apiUse     SuccessOKOldValue

@apiError   WrongInput    Reasons:
                            "Wrong input parameter(s) were provided",
                            "Argument 'location' has incorrect type (expected int, got str).

@apiUse     ErrorQueryDimmer
"""
@app.route('/valve/set', methods=['GET'], strict_slashes=False)
def knx_valve_set():
    content = request.get_json()
    if all(
            item in list(content.keys()) for item in [
                'group_address', 'value'
            ]
    ):
        group_address = str(content['group_address'])
        value = str(content['value'])

        command = backend + " " + "valve set " + group_address + " " + value

        output = subprocess.Popen(command, shell = True, stdout=subprocess.PIPE)
        result = str(output.stdout.read())

        return jsonify(result)

    else:
        return ("WrongInput -- Wrong input parameter(s) provided", 400)


#################################################################
"""
@api            {get}  /blind/open 
@apiName                knx_blind_open
@apiGroup               

@apiDescription Fully open the blinds

@apiParam {String} group_address, No action !!!

@apiParamExample {Object} Request example
    {
        'group_address' : '4/1',
    }

@apiUse     SuccessOKOldValue

@apiError   WrongInput    Reasons:
                            "Wrong input parameter(s) were provided",
                            "Argument 'location' has incorrect type (expected int, got str).

@apiUse     ErrorQueryDimmer
"""
@app.route('/blind/open', methods=['GET'], strict_slashes=False)
def knx_blind_open():
    content = request.get_json()
    if all(
            item in list(content.keys()) for item in [
                'group_address'
            ]
    ):
        group_address = str(content['group_address'])

        command = backend + " " + "blind open " + group_address

        output = subprocess.Popen(command, shell = True, stdout=subprocess.PIPE)
        result = str(output.stdout.read())

        return jsonify(result)

    else:
        return ("WrongInput -- Wrong input parameter(s) provided", 400)

#################################################################
"""
@api            {get}  /blind/close 
@apiName                knx_blind_close
@apiGroup               

@apiDescription Fully closes the blinds

@apiParam {String} group_address, No action !!!

@apiParamExample {Object} Request example
    {
        'group_address' : '4/1',
    }

@apiUse     SuccessOKOldValue

@apiError   WrongInput    Reasons:
                            "Wrong input parameter(s) were provided",
                            "Argument 'location' has incorrect type (expected int, got str).

@apiUse     ErrorQueryDimmer
"""
@app.route('/blind/close', methods=['GET'], strict_slashes=False)
def knx_blind_close():
    content = request.get_json()
    if all(
            item in list(content.keys()) for item in [
                'group_address'
            ]
    ):
        group_address = str(content['group_address'])

        command = backend + " " + "blind close " + group_address

        output = subprocess.Popen(command, shell = True, stdout=subprocess.PIPE)
        result = str(output.stdout.read())

        return jsonify(result)

    else:
        return ("WrongInput -- Wrong input parameter(s) provided", 400)

#################################################################
"""
@api            {get}  /blind/set set blind value
@apiName                knx_blind_set
@apiGroup               

@apiDescription Send a set command to a valve 

@apiParam {String} group_address, No action !!!
@apiParam {String} value, set [0,100] in %

@apiParamExample {Object} Request example
    {
        'group_address' : '4/1',
        'value' : '100'
    }

@apiUse     SuccessOKOldValue

@apiError   WrongInput    Reasons:
                            "Wrong input parameter(s) were provided",
                            "Argument 'location' has incorrect type (expected int, got str).

@apiUse     ErrorQueryDimmer
"""
@app.route('/blind/set', methods=['GET'], strict_slashes=False)
def knx_blind_set():
    content = request.get_json()
    if all(
            item in list(content.keys()) for item in [
                'group_address', 'value'
            ]
    ):
        group_address = str(content['group_address'])
        value = str(content['value'])

        command = backend + " " + "blind set " + group_address + " " + value

        output = subprocess.Popen(command, shell = True, stdout=subprocess.PIPE)
        result = str(output.stdout.read())

        return jsonify(result)

    else:
        return ("WrongInput -- Wrong input parameter(s) provided", 400)

#################################################################
"""
@api            {get}  /blind/get get blind value
@apiName                knx_valve_set
@apiGroup               

@apiDescription Send a set command to a valve 

@apiParam {String} group_address, No action !!!

@apiParamExample {Object} Request example
    {
        'group_address' : '4/1',
    }

@apiUse     SuccessOKOldValue

@apiError   WrongInput    Reasons:
                            "Wrong input parameter(s) were provided",
                            "Argument 'location' has incorrect type (expected int, got str).

@apiUse     ErrorQueryDimmer
"""
@app.route('/blind/get', methods=['GET'], strict_slashes=False)
def knx_blind_get():
    content = request.get_json()
    if all(
            item in list(content.keys()) for item in [
                'group_address'
            ]
    ):
        group_address = str(content['group_address'])

        command = backend + " " + "blind get " + group_address

        output = subprocess.Popen(command, shell = True, stdout=subprocess.PIPE)
        result = str(output.stdout.read())

        return jsonify(result)

    else:
        return ("WrongInput -- Wrong input parameter(s) provided", 400)

#################################################################
#################################################################

if __name__ == '__main__':

    if not my_name:
        raise RuntimeError("my_name not set")

    parser = argparse.ArgumentParser(
        description='Smart Building RESTful server',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        '-C, --ozw-config-path',
        dest='ozw_config_path',
        type=str,
        default='/etc/openzwave/',
        help='OZW configuration path -- see your backend lib installation'
    )

    parser.add_argument(
        '-U, --user-path',
        dest='user_path',
        type=str,
        default='~/tmp/OZW/',
        help='User path -- where all artifacts such as logs, etc., are put'
    )

    parser.add_argument(
        '-H, --host-name',
        type=str,
        dest='host_name',
        default='localhost',
        help='Our host-name or IP address'
    )

    parser.add_argument(
        '-p, --port',
        dest='port',
        type=int,
        default=5000,
        help='Our listening port'
    )

    parser.add_argument(
        '-l, --log-level',
        dest='log_level',
        type=str,
        default='warning',
        choices=('debug', 'info', 'warning', 'error', 'critical'),
        help='Logging level. On "debug" flask will auto-reload on file changes'
    )

    parser.add_argument(
        '-m', '--manual',
        dest='manual',
        action='store_true',
        help='print the full documentation'
    )

    parser.add_argument(
        '-R', '--reload',
        dest='reload',
        action='store_true',
        help='switch flask auto-reload on. Only effective when "log-level=debug".' +
        ' Beware of Bug <https://github.com/pallets/werkzeug/issues/1333>. Avoid!'
    )

    args = parser.parse_args()
    if args.manual:
        print(__doc__)
        sys.exit(0)

    log_level_n = getattr(logging, args.log_level.upper(), None)
    if not isinstance(log_level_n, int):
        raise ValueError('Invalid log level: {}'.format(args.log_level))


    # we put all artifacts here (frontend, backend, OZW libs...)
    user_path = os.path.expanduser(
            os.path.expandvars(
                args.user_path
            )
        )
    try:
        os.makedirs(user_path, exist_ok=True)
    except Exception as e:
        sys.exit("Can't create user_path: {}".format(e))

    fh = logging.FileHandler("{}/{}.log".format(user_path, my_name), mode='w')
    fh.setLevel(log_level_n)
    """fh.setFormatter(
        logging.Formatter(
            configpi.log_format_dbg if log_level_n <= logging.DEBUG else configpi.log_format
        )
    )"""

    app.logger.setLevel(log_level_n)
    app.logger.removeHandler(default_handler)
    app.logger.addHandler(fh)

    print("User path is '{}'".format(user_path))

    try:
        """backend = Backend_with_dimmers_and_sensors(
            ozw_config_path=args.ozw_config_path,
            ozw_user_path=user_path,
            log_level=log_level_n
        )
        try:
            backend.start()
            pass
        except Exception as e:
            sys.exit(e)
        """
        app.run(
            debug=True if log_level_n == logging.DEBUG else False,
            host=args.host_name,
            port=args.port,
            threaded=True,
            # beware: reloading doesn't call a backend.stop(), things may
            # break...
            use_reloader=True if args.reload and log_level_n == logging.DEBUG else False
        )

    except KeyboardInterrupt:
        backend.stop()
        print("Bye, bye!")
